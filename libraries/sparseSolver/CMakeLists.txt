set(sparseSolver_srcs
    linearSolver.cpp
    CGSolver.cpp
    PardisoSolver.cpp
    SPOOLESSolver.cpp
    invMKSolver.cpp
)
set(sparseSolver_hdrs
    CGSolver.h
    PardisoSolver.h
    SPOOLESSolver.h
    SPOOLESSolverMT.h
    invMKSolver.h
    linearSolver.h
    sparseSolverAvailability.h
    sparseSolvers.h
)

if (VegaFEM_BUILD_MODEL_REDUCTION)
  set(sparseSolver_srcs
      ${sparseSolver_srcs}
      ARPACKSolver.cpp
      invZTAZMSolver.cpp
      invZTAZSolver.cpp
      LagrangeMultiplierSolver.cpp
      ZTAZMultiplicator.cpp
  )
  set(sparseSolver_hdrs
      ${sparseSolver_hdrs}
      ARPACKSolver.h
      invZTAZMSolver.h
      invZTAZSolver.h
      LagrangeMultiplierSolver.h
      ZTAZMultiplicator.h
  )  
endif()

vega_add_library(sparseSolver
  SOURCES ${sparseSolver_srcs}
  PUBLIC_HEADERS ${sparseSolver_hdrs}
)

target_link_libraries(sparseSolver
  PUBLIC
    sparseMatrix
    # matrix
    performanceCounter
)

if (VegaFEM_BUILD_MODEL_REDUCTION)
target_link_libraries(sparseSolver
  PUBLIC
    matrix
)
endif()
